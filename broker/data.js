const mongoose = require('mongoose');

const { Schema } = mongoose;
const env = process.env;

mongoose.connect(`${env.DATABASE_URL}/data`);

const TempEvent = new Schema({
    temp: Number,
    humidity: Number
});

mongoose.model('TempEvent', TempEvent);

module.export = TempEvent;