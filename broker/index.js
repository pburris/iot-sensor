const mosca = require('mosca');
const TempEvent = require('./data');

const env = process.env;

const ascoltatore = {
  //using ascoltatore
  type: 'mongo',
  url: `${env.DATABASE_URL}/mqtt`,
  pubsubCollection: 'ascoltatori',
  mongo: {}
};

const settings = {
  port: 1883,
  backend: ascoltatore
};

const server = new mosca.Server(settings);

server.on('clientConnected', function(client) {
    console.log('client connected', client.id);
});

// fired when a message is received
server.on('published', function(packet, client) {
  console.log('Published', packet.payload.toString ? packet.payload.toString() : packet.payload);
});

server.on('ready', setup);

// fired when the mqtt server is ready
function setup() {
  console.log('Mosca server is up and running');
}