const wifi = require("Wifi");
const dht11 = require('DHT11');

const dht = dht11.connect(NodeMCU.D1);

const server = "52.87.180.51";
const mqtt = require("MQTT").create(server);

mqtt.on('connected', function() {
  console.log('MQTT connected...');
  mqtt.subscribe("temp");
});

mqtt.on('publish', function (pub) {
  console.log("topic: "+pub.topic);
  console.log("message: "+pub.message);
});

wifi.restore();
mqtt.connect();

function readDht11() {
  dht.read(t => {
    console.log(t);
    mqtt.publish('temp', t);
  });
}

setInterval(() => { readDht11(); }, 2500);